interface HttpRequestErrorOptions {
  status: number;
  message: string;
}

export class HttpRequestError extends Error {
  status: number;

  constructor(err: HttpRequestErrorOptions) {
    super(err.message);
    this.status = err.status;
  }
}

export class BadServerConfigurationError extends Error {
  constructor(key: string) {
    super(`Invalid configuration provided for '${key}'.`);
  }
}
