import { Router } from 'express';
import { auth } from './auth';
import { sendMailHandler } from './mail';


const v1Router = Router();

v1Router.use(auth);

v1Router.post('/mail/send', sendMailHandler);

export { v1Router };
