import { NextFunction, Request, Response } from 'express';
import { HttpRequestError } from '../../errors';
import { logger } from '../../services/logger';
import { EmailData, sendEmail } from '../../services/mail';

export async function sendMailHandler(req: Request, res: Response, next: NextFunction) {
  const data: EmailData = req.body;
  if (!data.emailRecipients || data.emailRecipients.length === 0) {
    return next(new HttpRequestError({
      status: 400,
      message: 'Invalid email recipients'
    }));
  }
  if (!data.emailBody) {
    return next(new HttpRequestError({
      status: 400,
      message: 'Invalid email body'
    }));
  }
  try {
    await sendEmail(data);
    logger.info(`Send mail attempt from ${data.emailSender} to ${data.emailRecipients}: SUCCESS`);
  } catch (e) {
    logger.info(`Send mail attempt from ${data.emailSender} to ${data.emailRecipients}: ERROR`);
    return next(new HttpRequestError({
      status: 500,
      message: e.message
    }));
  }
  res.status(200).send('success');
}
