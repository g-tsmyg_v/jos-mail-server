import { NextFunction, Request, Response } from 'express';
import { isTransporterInitialized } from '../services/mail';


enum ServerHealthStatus {
  UP = 'UP',
  DOWN = 'DOWN',
}

export async function healthCheck(req: Request, res: Response, next: NextFunction) {
  let serverStatus = ServerHealthStatus.UP;
  if (!(await isTransporterInitialized())) {
    serverStatus = ServerHealthStatus.DOWN;
  }
  return res.status(200).json({
    status: serverStatus,
  });
}
