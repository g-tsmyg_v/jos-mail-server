import { NextFunction, Request, Response } from 'express';
import * as zlib from 'zlib';
import { HttpRequestError } from '../errors';
import { logger } from '../services/logger';


const MS_IN_HOUR = 60 * 60 * 1000;


export function getLogs(req: Request, res: Response, next: NextFunction) {
  const {
    startDate,
    endDate,
    zipFlag
  } = req.query;
  const dateFrom: Date | any = new Date(Math.floor(startDate / MS_IN_HOUR) * MS_IN_HOUR);
  const dateUntil: Date | any = new Date(Math.ceil(endDate / MS_IN_HOUR) * MS_IN_HOUR);

  const logQueryOptions = {
    from: dateFrom.toString() !== 'Invalid Date' ? dateFrom : new Date(Date.now() - 2 * MS_IN_HOUR),
    until: dateUntil.toString() !== 'Invalid Date' ? dateUntil : new Date(),
    start: 0,
    order: 'desc' as 'desc',
    fields: undefined,
  };

  logger.query(logQueryOptions, (err, result) => {
    if (err) {
      logger.error('Log query error', err);
      return next(new HttpRequestError({
        status: 500,
        message: 'Log query error',
      }));
    }
    const jsonLog = JSON.stringify(result.dailyRotateFile);
    if (!zipFlag || zipFlag === '0') {
      return res.status(200).send(jsonLog);
    }
    const resultBuffer = Buffer.from(jsonLog, 'utf-8');
    zlib.gzip(resultBuffer, (gzipErr, zippedLog) => {
      if (gzipErr) {
        logger.error('Log query error', gzipErr);
        return next(new HttpRequestError({
          status: 500,
          message: 'Log query error',
        }));
      }
      res.status(200).send(zippedLog);
    });

  });

}
