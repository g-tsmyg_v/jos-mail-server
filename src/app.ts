import * as express from 'express';

import { apiRouter } from './api';
import { cors } from './cors';
import { HttpRequestError } from './errors';
import { logger } from './services/logger';

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors);

app.use('/api', apiRouter);

app.use('*', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  next(new HttpRequestError({
    status: 400,
    message: 'Route not found',
  }));
});

// Express error handler
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  logger.error(`${err.status || 500} - ${req.method} - ${req.originalUrl} - ${err.message} - ${req.ip}`);
  res.contentType('json');
  res.status(err.status || 500).send({
    message: err.message,
  });
});

export default app;
