FROM node:10 AS base
    RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
    WORKDIR /home/node/app
    COPY package*.json ./
    RUN npm install
    COPY . .
    COPY --chown=node:node . .
    USER node
    RUN npm run build
    EXPOSE 5000
    ENV PORT 5000
    CMD ["npm", "run", "build"]
    CMD ["npm", "start"]
